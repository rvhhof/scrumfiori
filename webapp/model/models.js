sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		
		createApplicationModel: function() {
			var oModel = new JSONModel({
				"board": {
					"todo": [{
						"issueId": "OSDC-7",
						"title": "Custom SVG control",
						"description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut justo sapien. Aliquam vehicula lorem a ante aliquam, et condimentum metus pharetra. Aliquam tincidunt leo vitae nisi faucibus, vel luctus quam dictum. Duis semper velit neque, quis efficitur tortor tristique eu. In id malesuada urna. Maecenas id risus interdum, hendrerit augue nec, condimentum tellus. Nullam ac dapibus quam, vitae dictum urna. In gravida auctor velit ut ornare. Duis blandit eu mi et tristique. Curabitur egestas, velit et dictum scelerisque, ex lacus laoreet ante, ac bibendum dui erat placerat ipsum. Aliquam porttitor convallis mollis. Ut commodo scelerisque mauris, sit amet dapibus ligula egestas eget. Vestibulum non pharetra sapien. Nulla efficitur quam lacus, vitae tristique purus semper sed. Curabitur bibendum nulla erat, maximus cursus tortor maximus id. Suspendisse a volutpat tortor. Fusce viverra dolor cursus vehicula consequat. Donec vitae dui condimentum, aliquam augue ut, dignissim arcu. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam consequat vehicula libero et scelerisque. Vestibulum consequat ligula id ipsum accumsan suscipit. Morbi iaculis tellus vitae augue finibus vulputate. Nam mattis tempor tempor. Donec augue augue, lobortis quis mi vel, consectetur blandit metus. Aliquam dictum purus nisl, vitae tincidunt dolor gravida sed.",
						"points": 2,
						"priority": 1,
						"assignedTo": {},
						"attachments": [],
						"notes": [],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:34:00.474Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:34:00.474Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}]
					}, {
						"issueId": "OSDC-6",
						"title": "Custom Theme and CSS",
						"description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut justo sapien. Aliquam vehicula lorem a ante aliquam, et condimentum metus pharetra. Aliquam tincidunt leo vitae nisi faucibus, vel luctus quam dictum. Duis semper velit neque, quis efficitur tortor tristique eu. In id malesuada urna. Maecenas id risus interdum, hendrerit augue nec, condimentum tellus. Nullam ac dapibus quam, vitae dictum urna. In gravida auctor velit ut ornare. Duis blandit eu mi et tristique. Curabitur egestas, velit et dictum scelerisque, ex lacus laoreet ante, ac bibendum dui erat placerat ipsum. Aliquam porttitor convallis mollis. Ut commodo scelerisque mauris, sit amet dapibus ligula egestas eget. Vestibulum non pharetra sapien. Nulla efficitur quam lacus, vitae tristique purus semper sed. Curabitur bibendum nulla erat, maximus cursus tortor maximus id. Suspendisse a volutpat tortor. Fusce viverra dolor cursus vehicula consequat. Donec vitae dui condimentum, aliquam augue ut, dignissim arcu. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam consequat vehicula libero et scelerisque. Vestibulum consequat ligula id ipsum accumsan suscipit. Morbi iaculis tellus vitae augue finibus vulputate. Nam mattis tempor tempor. Donec augue augue, lobortis quis mi vel, consectetur blandit metus. Aliquam dictum purus nisl, vitae tincidunt dolor gravida sed.",
						"points": 5,
						"priority": 1,
						"assignedTo": {
							"gender": "female",
							"name": "Monique Jolie",
							"picture": {
								"large": "https://randomuser.me/api/portraits/women/46.jpg",
								"medium": "https://randomuser.me/api/portraits/med/women/46.jpg",
								"thumbnail": "https://randomuser.me/api/portraits/thumb/women/46.jpg"
							}
						},
						"attachments": [{
							"fileName": "Technical Documentation.docx",
							"mimeType": "application/msword",
							"attributes": [{
								"title": "Uploaded By",
								"text": "Roberto Clarke"
							}, {
								"title": "Uploaded On",
								"text": "2016-04-12"
							}, {
								"title": "File Size",
								"text": "1.3Mb"
							}]
						}, {
							"fileName": "DataModel_v2.xlsx",
							"mimeType": "application/msexcel",
							"attributes": [{
								"title": "Uploaded By",
								"text": "Monique Jolie"
							}, {
								"title": "Uploaded On",
								"text": "2016-04-14"
							}, {
								"title": "File Size",
								"text": "386Kb"
							}]
						}],
						"notes": [{
							"title": "One small remark",
							"description": "Lorem ipsum dolor sit amet",
							"author": "Brennan Miles",
							"datetime": "3 days"
						}, {
							"title": "RE: One small remark",
							"description": "You are right, but ipsum dolor sit amet",
							"author": "Monique Jolie",
							"datetime": "3 days"
						}, {
							"title": "RE: One small remark",
							"description": "Ok, understand :-)",
							"author": "Jane Doe",
							"datetime": "3 days"
						}],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T15:33:55.965Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T15:33:55.965Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "assigned Issue to Monique Jolie",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T15:33:55.965Z"),
							"icon": "sap-icon://activity-individual"
						}]
					}, {
						"issueId": "OSDC-5",
						"title": "Enable Sorting and Grouping ",
						"description" : "",
						"points": 2,
						"priority": 2,
						"assignedTo": {},
						"attachments": [],
						"notes": [],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:43:53.610Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:43:53.610Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}]
					}, {
						"issueId": "OSDC-9",
						"title": "Update Documentation",
						"description" : "",
						"points": 2,
						"priority": 2,
						"assignedTo": {
							"gender": "male",
							"name": "Jeremy Evans",
							"picture": {
								"large": "https://randomuser.me/api/portraits/men/84.jpg",
								"medium": "https://randomuser.me/api/portraits/med/men/84.jpg",
								"thumbnail": "https://randomuser.me/api/portraits/thumb/men/84.jpg"
							}
						},
						"attachments": [],
						"notes": [],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T12:21:53.610Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T12:21:53.610Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "assigned Issue to Jeremy Evans",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T17:49:28.610Z"),
							"icon": "sap-icon://activity-individual"
						}]
					}, {
						"issueId": "OSDC-8",
						"title": "iOS Kapsel build",
						"description" : "",
						"points": 2,
						"priority": 1,
						"assignedTo": {
							"gender": "male",
							"name": "Roberto Clarke",
							"picture": {
								"large": "https://randomuser.me/api/portraits/men/31.jpg",
								"medium": "https://randomuser.me/api/portraits/med/men/31.jpg",
								"thumbnail": "https://randomuser.me/api/portraits/thumb/men/31.jpg"
							}
						},
						"attachments": [],
						"notes": [],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T14:22:55.687Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T14:22:55.687Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "assigned Issue to Roberto Clarke",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T14:22:55.687Z"),
							"icon": "sap-icon://activity-individual"
						}]
					}],
					"inProgress": [{
						"issueId": "OSDC-4",
						"title": "Create Detail view",
						"description" : "",
						"points": 3,
						"priority": 1,
						"assignedTo": {
							"gender": "male",
							"name": "Brennan Miles",
							"picture": {
								"large": "https://randomuser.me/api/portraits/men/57.jpg",
								"medium": "https://randomuser.me/api/portraits/med/men/57.jpg",
								"thumbnail": "https://randomuser.me/api/portraits/thumb/men/57.jpg"
							}
						},
						"attachments": [],
						"notes": [],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:33:35.515Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:33:35.515Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "assigned Issue to Brennan Miles",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:33:35.515Z"),
							"icon": "sap-icon://activity-individual"
						}, {
							"title": "set Status to IN PROGRESS",
							"text": "",
							"userName": "Brennan Miles",
							"dateTime": new Date("2016-04-12T09:12:21.834Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}]
					}],
					"test": [{
						"issueId": "OSDC-3",
						"title": "Create Master view",
						"points": 2,
						"priority": 1,
						"assignedTo": {
							"gender": "female",
							"name": "Andrea Fernandez",
							"picture": {
								"large": "https://randomuser.me/api/portraits/women/67.jpg",
								"medium": "https://randomuser.me/api/portraits/med/women/67.jpg",
								"thumbnail": "https://randomuser.me/api/portraits/thumb/women/67.jpg"
							}
						},
						"attachments": [],
						"notes": [],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:33:35.515Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T16:33:35.515Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "assigned Issue to Andrea Fernandez",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T17:01:20.515Z"),
							"icon": "sap-icon://activity-individual"
						}, {
							"title": "set Status to IN PROGRESS",
							"text": "",
							"userName": "Andrea Fernandez",
							"dateTime": new Date("2016-04-12T09:01:30.491Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "set Status to TEST",
							"text": "",
							"userName": "Andrea Fernandez",
							"dateTime": new Date("2016-04-12T17:33:38.802Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}]
					}, {
						"issueId": "OSDC-2",
						"title": "Mock Server configuration",
						"points": 5,
						"priority": 1,
						"assignedTo": {
							"gender": "female",
							"name": "Monique Jolie",
							"picture": {
								"large": "https://randomuser.me/api/portraits/women/46.jpg",
								"medium": "https://randomuser.me/api/portraits/med/women/46.jpg",
								"thumbnail": "https://randomuser.me/api/portraits/thumb/women/46.jpg"
							}
						},
						"attachments": [],
						"notes": [],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T20:34:00.474Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T20:34:00.474Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "assigned Issue to Monique Jolie",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T20:34:00.474Z"),
							"icon": "sap-icon://activity-individual"
						}, {
							"title": "set Status to IN PROGRESS",
							"text": "",
							"userName": "Monique Jolie",
							"dateTime": new Date("2016-04-12T09:14:21.491Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "set Status to TEST",
							"text": "",
							"userName": "Monique Jolie",
							"dateTime": new Date("2016-04-12T18:33:21.802Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}]
					}],
					"done": [{
						"issueId": "OSDC-1",
						"title": "Create App Shell",
						"points": 3,
						"priority": 1,
						"assignedTo": {
							"gender": "male",
							"name": "Roberto Clarke",
							"picture": {
								"large": "https://randomuser.me/api/portraits/men/31.jpg",
								"medium": "https://randomuser.me/api/portraits/med/men/31.jpg",
								"thumbnail": "https://randomuser.me/api/portraits/thumb/men/31.jpg"
							}
						},
						"attachments": [{
							"fileName": "Technical Documentation.docx",
							"mimeType": "application/msword",
							"attributes": [{
								"title": "Uploaded By",
								"text": "Jane Burns"
							}, {
								"title": "Uploaded On",
								"text": "2014-07-28"
							}, {
								"title": "File Size",
								"text": "1.3Mb"
							}]
						}, {
							"fileName": "DataModel_v2.xlsx",
							"mimeType": "application/msexcel",
							"attributes": [{
								"title": "Uploaded By",
								"text": "Jane Burns"
							}, {
								"title": "Uploaded On",
								"text": "2014-07-28"
							}, {
								"title": "File Size",
								"text": "1.3Mb"
							}]
						}],
						"notes": [{
							"title": "One small remark",
							"description": "Lorem ipsum dolor sit amet",
							"author": "Jane Doe",
							"datetime": "3 days"
						}, {
							"title": "RE: One small remark",
							"description": "You are right, but ipsum dolor sit amet",
							"author": "Monique Doe",
							"datetime": "3 days"
						}, {
							"title": "RE: One small remark",
							"description": "Ok, understand :-)",
							"author": "Jane Doe",
							"datetime": "3 days"
						}],
						"history": [{
							"title": "created the Issue",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T10:21:00.474Z"),
							"icon": "sap-icon://add-activity-2"
						}, {
							"title": "set Status to TODO",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T10:21:00.474Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "assigned Issue to Roberto Clarke",
							"text": "",
							"userName": "Jeremy Evans",
							"dateTime": new Date("2016-04-09T10:21:00.474Z"),
							"icon": "sap-icon://activity-individual"
						}, {
							"title": "set Status to IN PROGRESS",
							"text": "",
							"userName": "Roberto Clarke",
							"dateTime": new Date("2016-04-12T10:32:12.075Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "set Status to TEST",
							"text": "",
							"userName": "Roberto Clarke",
							"dateTime": new Date("2016-04-12T13:49:27.637Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}, {
							"title": "set Status to DONE",
							"text": "",
							"userName": "Roberto Clarke",
							"dateTime": new Date("2016-04-12T20:32:22.031Z"),
							"icon": "sap-icon://activity-assigned-to-goal"
						}]
					}]
				},
				"burndown": [],
				"bugs": {
					"active": [],
					"fixed": []
				},
				"settings": {
					"projectName": "openSAP Design Challenge",
					"issuePrefix": "OSDC",
					"startDate": "Apr 4, 2016",
					"endDate": "Apr 22, 2016",
					"counter": 10
				},
				"ui": {
					"issue": {
						"title": null,
						"issueId": null,
						"points": 0,
						"priority": 3,
						"assignedTo": null,
						"attachments" : [],
						"notes"       : [],
						"history"     : []
					},
					"settings": {},
					"loggedInUser": "Monique Jolie"
				},
				"users": [{
				}, {
					"gender": "male",
					"name": "Brennan Miles",
					"picture": {
						"large": "https://randomuser.me/api/portraits/men/57.jpg",
						"medium": "https://randomuser.me/api/portraits/med/men/57.jpg",
						"thumbnail": "https://randomuser.me/api/portraits/thumb/men/57.jpg"
					}
				}, {
					"gender": "female",
					"name": "Monique Jolie",
					"picture": {
						"large": "https://randomuser.me/api/portraits/women/46.jpg",
						"medium": "https://randomuser.me/api/portraits/med/women/46.jpg",
						"thumbnail": "https://randomuser.me/api/portraits/thumb/women/46.jpg"
					}
				}, {
					"gender": "male",
					"name": "Jeremy Evans",
					"picture": {
						"large": "https://randomuser.me/api/portraits/men/84.jpg",
						"medium": "https://randomuser.me/api/portraits/med/men/84.jpg",
						"thumbnail": "https://randomuser.me/api/portraits/thumb/men/84.jpg"
					}
				}, {
					"gender": "male",
					"name": "Roberto Clarke",
					"picture": {
						"large": "https://randomuser.me/api/portraits/men/31.jpg",
						"medium": "https://randomuser.me/api/portraits/med/men/31.jpg",
						"thumbnail": "https://randomuser.me/api/portraits/thumb/men/31.jpg"
					}
				}, {
					"gender": "female",
					"name": "Vanessa Tucker",
					"picture": {
						"large": "https://randomuser.me/api/portraits/women/60.jpg",
						"medium": "https://randomuser.me/api/portraits/med/women/60.jpg",
						"thumbnail": "https://randomuser.me/api/portraits/thumb/women/60.jpg"
					}
				}, {
					"gender": "female",
					"name": "Andrea Fernandez",
					"picture": {
						"large": "https://randomuser.me/api/portraits/women/67.jpg",
						"medium": "https://randomuser.me/api/portraits/med/women/67.jpg",
						"thumbnail": "https://randomuser.me/api/portraits/thumb/women/67.jpg"
					}
				}]
			});
			
			return oModel;
		}

	};

});