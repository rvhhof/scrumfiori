sap.ui.define([

], function () {
    "use strict";
    
    return {

        formatPriority: function(iPriority) {
            switch (iPriority) {
                case 1:
                    return "High";
                case 2:
                    return "Medium";
                case 3:
                    return "Low";
            }
        },
        
        formatPriorityState: function(iPriority) {
            switch (iPriority) {
                case 1:
                    return "Error";
                case 2:
                    return "Warning";
                case 3:
                    return "None";
            }
        },
        
        formatAssignedTo: function(sValue) {
        	if (sValue) {
	        	return "Assigned to: " + sValue;
        	}
        	else {
        		return "";
        	}
        },

        formatTotalStoryPoints: function(aData) {
            return aData.reduce( function(a, b){  
                return a + parseFloat(b.points);  
            }, 0); 
        },
        
        formatDaysRemaining: function(sEndDate) {
        	var oEndDate             = new Date(sEndDate),
        		now                  = new Date(),
        		iNumWeekendDays      = this._getNumberOfWeekendDays(now, oEndDate),
        		iTotalDaysRemaining  = 0;
        		
        	now.setHours(0, 0, 0, 0);

            iTotalDaysRemaining = Math.round((oEndDate.getTime() - now.getTime()) / 864e5);
    
            if (iTotalDaysRemaining > 0) {
            	return iTotalDaysRemaining - iNumWeekendDays + 1;
            }
            else {
                return 0; // return 0 when current date is after sprint end date
            }

        }

    };
});