# ScrumFiori

Thank you for viewing and evaluating my submission for the **[openSAP "Build Your Own SAP Fiori App in the Cloud – 2016 Edition"](https://open.sap.com/courses/fiux2)**  Develop Challenge.

### Note when using this app
Quite possibly, you will be viewing this app long after the Develop Challenge submission date (mid-April 2016). Because it was developed for this Develop Challenge, and since the app uses mocked data, the app runs with a predefined Sprint period set between April 4 and April 22, 2016.

Therefore, you will see the sprint burndown graph prolonging until the end of the sprint, and the 'days remaining' panel will show `0`, effectifely meaning the sprint has finished (without delivering much...)

_In order to simulate a "real-life, mid-sprint" situation, I would advice setting a sprint start date approximately one week before the current date, and the sprint end date to one or two weeks after the current date._

_You can change the Sprint period by clicking the **Project Settings** button at the top right._

If you now go to the Scrumboard by clicking the green ***Go to scrumboard*** button, drag a story to the ***DONE*** lane, and navigate back to the dashboard, you will see the burndown chart reflect the changes (i.e. the amound of story points of the item you dragged to done is now deducted from the burndown graph)

### Known issues
Gazillions, since it was just intended for the openSAP Design / Develop Challenge. Currently there is no real, multi-user, multi-sprint, multi-project version planned (but you'll never know)  
As for the Story Details page, except for status updates in the ***History*** tab, nothing will reflect any actions you will perform (edits, attachment uploads, create notes, etc)

### About this app

Created using [SAPUI5](https://sapui5.hana.ondemand.com/sdk/), developed with [SAP WebIDE](https://eaexplorer.hana.ondemand.com/_item.html?id=10667#!/overview), inspired by [SAP Fiori UX](https://eaexplorer.hana.ondemand.com/_item.html?id=10610#!/overview)

Dummy team members were created using the open-source [Random User Generator](https://randomuser.me)

This help text's markup is done using [Showdown](https://github.com/showdownjs/showdown), a Javascript port of Markdown