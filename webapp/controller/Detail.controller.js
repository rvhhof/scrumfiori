sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
    "nl/qualiture/scrumfiori/util/Formatter"
], function(Controller, History, Formatter) {
	"use strict";

	return Controller.extend("nl.qualiture.scrumfiori.controller.Detail", {

		formatter : Formatter,

		onInit: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);

			this.setUISizeCompactStyleClass(this.getView());
		},

		setUISizeCompactStyleClass: function(oControl) {
            if (!sap.ui.Device.support.touch) {
                oControl.addStyleClass("sapUiSizeCompact");
            }
		},
		
		navBack: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this),
				oHistory, sPreviousHash;
			
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				oRouter.navTo("scrumboard", {}, true /*no history*/);
			}
		},
		
		_onObjectMatched: function (oEvent) {
			var issuePath = oEvent.getParameter("arguments").issuePath.split("_").join("/");
			
			this.getView().bindElement({
				path: "/" + issuePath
			});
		}
		
	});

});