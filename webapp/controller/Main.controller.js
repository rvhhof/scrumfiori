/* global showdown */

sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "nl/qualiture/scrumfiori/util/Formatter"
], function(Controller, Formatter) {
	"use strict";

    $.sap.require("sap.ui.core.format.DateFormat");
    $.sap.require("nl.qualiture.scrumfiori.resources.showdown");

	return Controller.extend("nl.qualiture.scrumfiori.controller.Main", {

		formatter        : Formatter,

		_oDialogIssue    : null,
		_oDialogSettings : null,
		_oDialogHelp     : null,
		
		onInit: function() {
			this.setUISizeCompactStyleClass(this.getView());
		},
		
		onAfterRendering: function() {
            this.getView().getModel().setProperty("/burndown", this._generateBurndownData());
		},
		
		setAssignedUser: function(oEvent) {
			var oModel  = this.getView().getModel(),
				obj     = oEvent.getParameter("selectedItem").getBindingContext().getObject();
			
			oModel.setProperty("/ui/issue/assignedTo", obj);
		},
		
        openDialogIssue: function(oEvent) {
            if (!this._oDialogIssue) {
                this._oDialogIssue = sap.ui.xmlfragment("nl.qualiture.scrumfiori.fragment.Issue", this);
                this.setUISizeCompactStyleClass(this._oDialogIssue);
                this.getView().addDependent(this._oDialogIssue);
            }

			var oModel  = this.getView().getModel();
			oModel.setProperty("/ui/issue/issueId", this.calculatedIssueID());
			
            this._oDialogIssue.open();
        },

		addDialogIssue: function() {
			var oModel   = this.getView().getModel();
			var aIssues  = oModel.getProperty("/bugs/active");
			var aTodo    = oModel.getProperty("/board/todo");
			var iCounter = oModel.getProperty("/settings/counter");
			var oIssue   = oModel.getProperty("/ui/issue");
			var sMe      = oModel.getProperty("/ui/loggedInUser");
			var aHistory = [
				{
					title       : "created the Issue",
					text        : "",
					userName    : sMe,
					dateTime    : new Date(),
					icon        : "sap-icon://add-activity-2"
				},
				{
					title       : "set Status to TODO",
					text        : "",
					userName    : sMe,
					dateTime    : new Date(),
					icon        : "sap-icon://activity-assigned-to-goal"
				},
				{
					title       : "assigned Issue to " + oIssue.assignedTo,
					text        : "",
					userName    : sMe,
					dateTime    : new Date(),
					icon        : "sap-icon://activity-individual"
				}
			];	
			
			oIssue.history = aHistory;
			
			aIssues.push(oIssue);
			aTodo.push(oIssue);
			
			oModel.setProperty("/bugs/active", aIssues);
			oModel.setProperty("/board/todo", aTodo);
			oModel.setProperty("/settings/counter", ++iCounter);
			oModel.setProperty("/burndown", this._generateBurndownData());
			
			oModel.setProperty("/ui/issue", {
				title      : null,
				issueId    : null,
				points     : 0,
				priority   : 3,
				assignedTo : null
			});
			
			this.closeDialogIssue();
		},
		
        closeDialogIssue : function() {
            this._oDialogIssue.close();
        },

        openDialogSettings: function() {
        	var oModel = this.getView().getModel(),
        		newObj = {};
        	
            if (!this._oDialogSettings) {
                this._oDialogSettings = sap.ui.xmlfragment("nl.qualiture.scrumfiori.fragment.Settings", this);
                this.setUISizeCompactStyleClass(this._oDialogSettings);
                this.getView().addDependent(this._oDialogSettings);
            }

			newObj = $.extend(true, {}, oModel.getProperty("/settings"));
			
			oModel.setProperty("/ui/settings", newObj);
			
            this._oDialogSettings.open();
        },

		saveSettings: function() {
			var oModel = this.getView().getModel(),
				newObj = {};

			newObj = $.extend(true, {}, oModel.getProperty("/ui/settings"));
			
			oModel.setProperty("/settings", newObj);
			oModel.setProperty("/ui/settings", {});
			
			oModel.setProperty("/burndown", this._generateBurndownData());
			
			this.closeDialogSettings();
		},
		
        closeDialogSettings: function() {
            this._oDialogSettings.close();
        },

        openDialogHelp: function(oEvent) {
            if (!this._oDialogHelp) {
                this._oDialogHelp = sap.ui.xmlfragment("nl.qualiture.scrumfiori.fragment.Help", this);
                this.loadDialogHelpContent();
                this.setUISizeCompactStyleClass(this._oDialogHelp);
                this.getView().addDependent(this._oDialogHelp);
            }

            this._oDialogHelp.open();
        },

        /**
         * I have added a 'help' dialog, explaining how you could use this
         * scrumboard after the sprint period created for the Develop Challenge
         * has long gone.
         * This method loads the Markdown-formatted description, and sets it to the content 
         * of the core:HTML element
         * 
         * @memberOf nl.qualiture.scrumfiori.controller.Main
         */
        loadDialogHelpContent: function() {
            var oControl = sap.ui.getCore().byId("markdownContainer");
            if (oControl) {
                $.ajax({
                    url : "resources/readme.md",
                    dataType: "text",
                    success : function (data) {
                        var converter = new showdown.Converter();
                        oControl.setContent(converter.makeHtml(data));
                    }
                });
            }
        },
        
        closeDialogHelp: function() {
            this._oDialogHelp.close();
        },

		setUISizeCompactStyleClass: function(oControl) {
            if (!sap.ui.Device.support.touch) {
                oControl.addStyleClass("sapUiSizeCompact");
            }
		},
		
		navToScrumboard: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("scrumboard"); 
		},
		
		openDetails: function(oEvent) {
			//TODO: Implement
		},
		
		calculatedIssueID: function() {
			var oModel = this.getView().getModel();
			
			return oModel.getProperty("/settings/issuePrefix") + "-" + oModel.getProperty("/settings/counter");
		},
		
		_generateBurndownData: function() {
            var aData         = [],
                obj           = {},
                totalPoints   = 26,  // This value should normally come from the aggrgation of all stories for the given sprint, calculated prior to the start of the sprint. This is now hard-coded
                burntPoints   = totalPoints,
                sprintStart   = new Date(this.getView().getModel().getProperty("/settings/startDate")),
                sprintEnd     = new Date(this.getView().getModel().getProperty("/settings/endDate")),
                dateFormat    = sap.ui.core.format.DateFormat.getTimeInstance({pattern: "MMM d"}),
                totalDays, sprintDays, weekendDays, currentPoints, currentDay, today, 
                sprintDayCounter = 0,
                aHistory      = [];

            // calculate the total sprint days (total days minus the weekends (and ultimately, holidays of course)) 
            totalDays   = this._getNumberOfSprintDays(sprintStart, sprintEnd);
            weekendDays = this._getNumberOfWeekendDays(sprintStart, sprintEnd);
            sprintDays  = totalDays - weekendDays;

            // start calculating the burndownchart data
            currentDay = new Date(sprintStart.getTime());
            today = new Date();
            today.setHours(0, 0, 0, 0); // remove time information

            aHistory = this._getDoneItemsFlattened();
            
            for (var i=0; i<totalDays; i++) {
                if (currentDay.getDay() > 0 && currentDay.getDay() < 6) {
                    currentPoints = (totalPoints - (totalPoints * (sprintDayCounter / (sprintDays - 1)))).toFixed(2); // round points to 2 decimals for 'smooth'
                    sprintDayCounter++;
                }

                obj = {
                    date : dateFormat.format(new Date(currentDay.getTime())),
                    day : i,
                    plannedPoints : currentPoints
                };

                // console.log(this._getBurntPointsAtDay(aHistory, currentDay));
                
				// if (currentDay.getDate() === 12) {
				// 	burntPoints -= 3;
				// }
				burntPoints -= this._getBurntPointsAtDay(aHistory, currentDay);
				
                if (today.getTime() >= currentDay.getTime()) {
                    obj.remainingPoints = burntPoints;
                }

                aData.push(obj);

                currentDay.setDate(currentDay.getDate() + 1);
            }

            return aData;
		},
		
		_getDoneItemsFlattened : function() {
		    var aItemsDone      = this.getView().getModel().getProperty("/board/done");
		    var aHistory        = aItemsDone.map(function(e){ 
		        var oDateDone = e.history.filter(function(obj) {
		            return obj.title === "set Status to DONE";                         
		        })[0];
		        return { history: new Date(oDateDone.dateTime.setHours(0, 0, 0, 0)), points: e.points} ; 
		        
		    });
		    var aHistoryFlatten = [].concat.apply([], aHistory);
		    
		    return aHistoryFlatten;
		},
		
		_getBurntPointsAtDay : function(aItems, oDate) {
		    var iTotalPoints = 0;
		    var aPoints = [];
		    var oItem = aItems.filter(function(obj) {
		        return obj.history.getTime() === oDate.getTime();
		    });
		    if (oItem.length) {
		        aPoints = oItem.map(function(e) {
		            return e.points;
		        });
		        iTotalPoints = aPoints.reduce(function(a, b) { 
		            return a + b; 
		        }, 0);
		    }
		    return iTotalPoints;
		},
		
        _getNumberOfSprintDays : function(d1, d2) {
            return ((d2 - d1) / (1000 * 60 * 60 * 24)) + 1;
        },

        _getNumberOfWeekendDays : function(d1, d2) {
            var isWeekend = false,
                totalDays = 0,
                start     = new Date(d1.getTime());

            while (start < d2) {
                var day = start.getDay();
                isWeekend = (day === 6) || (day === 0); 
                if (isWeekend) { totalDays++; }
                start.setDate(start.getDate() + 1);
            }
            return totalDays;            
        }
	});

});