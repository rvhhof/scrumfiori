sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
    "nl/qualiture/scrumfiori/util/Formatter"
], function(Controller, History, Formatter) {
	"use strict";

    $.sap.require("sap.ui.core.format.DateFormat");
    $.sap.require("sap.ui.thirdparty.jqueryui.jquery-ui-core");
    $.sap.require("sap.ui.thirdparty.jqueryui.jquery-ui-widget");
    $.sap.require("sap.ui.thirdparty.jqueryui.jquery-ui-mouse");
    $.sap.require("sap.ui.thirdparty.jqueryui.jquery-ui-draggable");
    $.sap.require("sap.ui.thirdparty.jqueryui.jquery-ui-droppable");
    $.sap.require("sap.ui.thirdparty.jqueryui.jquery-ui-sortable");

	return Controller.extend("nl.qualiture.scrumfiori.controller.Scrumboard", {

		formatter : Formatter,

		_oDialogIssue  : null,
		_oDialogSettings  : null,
		
		onInit: function() {
			this.setUISizeCompactStyleClass(this.getView());
			
        	var listTodo       = this.byId("listTodo"),
        	    listInProgress = this.byId("listInProgress"),
        	    listTest       = this.byId("listTest"),
        	    listDone       = this.byId("listDone"),
        	    self           = this,
        	    oModel         = this.getOwnerComponent().getModel();

        	listTodo.onAfterRendering = function() {
        		self.setListEnableDragDrop(this, oModel, self);
        	};
        	listInProgress.onAfterRendering = function() {
        		self.setListEnableDragDrop(this, oModel, self);
        	};
        	listTest.onAfterRendering = function() {
        		self.setListEnableDragDrop(this, oModel, self);
        	};
        	listDone.onAfterRendering = function() {
        		self.setListEnableDragDrop(this, oModel, self);
        	};
		},

		onAfterRendering: function() {
			
		},
		
		setUISizeCompactStyleClass: function(oControl) {
            if (!sap.ui.Device.support.touch) {
                oControl.addStyleClass("sapUiSizeCompact");
            }
		},
		
		navBack: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this),
				oHistory, sPreviousHash;
			
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				oRouter.navTo("main", {}, true /*no history*/);
			}
		},
		
		navToIssueDetails: function(oEvent) {
			var oRouter  = sap.ui.core.UIComponent.getRouterFor(this),
				oIssue   = oEvent.getSource(),
				sPath    = oIssue.getBindingContext().getPath().substr(1).split("/").join("_");
				
			oRouter.navTo("detail", { issuePath : sPath });
		},
		
		/**
		 * This is the fun part! I am planning to write a small blog
		 * about using drag/drop in conjunction with updating the model
		 * based on the drag/drop interactions
		 * 
		 */
		setListEnableDragDrop : function(oList, oModel, oController) {
    		if (sap.m.List.prototype.onAfterRendering) {
    			sap.m.List.prototype.onAfterRendering.apply(oList);
    		}

    		oList.$().children().sortable({ 
    		    helper : "clone",
    		    appendTo : "body",
    		    connectWith : ".ui-sortable",
    		    start : function (event, ui) {
        		    var sSourceListId  = oList.getId();
        			var droppedElement = sap.ui.getCore().byId(ui.item.context.id);
        			
        			droppedElement.data("source-list-id", sSourceListId);
        			droppedElement.data("source-index", ui.item.index());
    		    },
    		    update : function (event, ui) {
    		        var newIndex         = ui.item.index();
        			var droppedElement   = sap.ui.getCore().byId(ui.item.context.id);
        			var context          = droppedElement.getBindingContext();
        			var object           = context.getObject();
                    var sListBindingPath = oList.getBindingPath("items");
        			var sStatus          = "";
    				var sMe              = oModel.getProperty("/ui/loggedInUser");
        			
                    if (droppedElement.data("target-list-id") === oList.getId()) {
            			var aTarget     = oModel.getProperty(sListBindingPath);
            			var sTargetLane = (sListBindingPath.split("/"))[2];
            			
            			if (sTargetLane === "todo")            { sStatus = "TODO"; }
            			else if (sTargetLane === "inProgress") { sStatus = "IN PROGRESS"; }
            			else if (sTargetLane === "test")       { sStatus = "TEST"; }
            			else if (sTargetLane === "done")       { 
            				sStatus = "DONE"; 
            				
            				var aBurndown = oModel.getProperty("/burndown");
            				var index     = oController._getIndexOfTodaysBurndownItem(aBurndown);
            				var oBurndown = aBurndown[index];
            				
            				oBurndown.remainingPoints -=  object.points;                
            				aBurndown[index] = oBurndown;
            				oModel.setProperty("/burndown", aBurndown);
            			}
    
                        // a 'move' to a different lane        			
            			if (droppedElement.data("target-list-id") !== droppedElement.data("source-list-id")) {
        					object.history.push({
        						title       : "set Status to " + sStatus,
        						text        : "",
        						userName    : sMe,
        						dateTime    : new Date(),
        						icon        : "sap-icon://activity-assigned-to-goal"
        					});
        
                            // save to target list
                			aTarget.splice(newIndex, 0, object);
                			oModel.setProperty(sListBindingPath, aTarget);
                			
                			// remove from source list
                			var sSourceListBindingPath = sap.ui.getCore().byId(droppedElement.data("source-list-id")).getBindingPath("items");
                			var aSource = oModel.getProperty(sSourceListBindingPath);
                			aSource.splice(droppedElement.data("source-index"), 1);
                			oModel.setProperty(sSourceListBindingPath, aSource);
            			}
            			// a 'sort' in the same lane
            			else {
            			    aTarget.splice(newIndex, 0, aTarget.splice(droppedElement.data("source-index"), 1)[0]);
                			oModel.setProperty(sListBindingPath, aTarget);
                			// For some reason, sorting and subsequent model updating *in the same list* did not work 
                			// the way I expected. The solution was to
                			// - Update the model to the way it should be sorted
                			// - Cancel the jQuery sort
                			// This way, the model now reflects the correct changes (and not the jQuery sort order)
                			$(this).sortable("cancel");
            			}

                        // clean up data attributes
            			droppedElement.data("source-list-id", undefined);
            			droppedElement.data("source-index", undefined);
            			droppedElement.data("target-list-id", undefined);
            			
            			
                    }
    		    },
                stop : function() {
                    oModel.refresh(true);    		    
                }
    		}).disableSelection();
    		
        	oList.$().droppable({
        		drop : function (event, ui) {
        		    var sTargetListId  = oList.getId();
        			var droppedElement = sap.ui.getCore().byId(ui.draggable.context.id);
        			droppedElement.data("target-list-id", sTargetListId);
        		}
        	});		
		},

        _getIndexOfTodaysBurndownItem : function(aBurndown) {
            var dateFormat = sap.ui.core.format.DateFormat.getTimeInstance({pattern: "MMM d"}),
                now        = new Date(),
                index;

            index = aBurndown.map(function(obj) {
			    return obj.date;
			}).indexOf(dateFormat.format(now));
			
			return index > -1 ? index : aBurndown.length -1; // NOTE: Fallback to last item in burndown array in case today does not 
			                                                 // fall within sprint start/end dates.
			                                                 // This is not a real-life scenario since you cannot work on sprints in 
			                                                 // the past of future, but for the POC demo I left it in
        }

	});

});